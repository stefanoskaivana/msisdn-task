<?php

use MSISDNDecoder\MSISDN;


class MSISDNTest extends PHPUnit_Framework_TestCase
{
    private $testObj = null;

    public function setUp(){
        $this->testObj = new MSISDN("");
    }

    public function tearDown() {
        $this->testObj = null;
    }

    /**
     * @dataProvider sanitizeProvider
     */
    public function testSanitize($input,$expected){

        $result = $this->testObj->Sanitize($input);
        $this->assertSame($expected, $result);
    }

    public function sanitizeProvider() // I have utilized data provider to avoid code duplication. Instead of writing several test methods for essentially the same method. I now have only one test method.
    {
        return array(
            "blank spaces pass"  => array("389 77 76 75 07", "38977767507"),
//            "blank spaces fail"  => array("389 77 76 75 07", "389 77 76 75 07"),
            "+ prefix pass" => array("+38977767507", "38977767507"),
//            "+ prefix fail" => array("+38977767507", "+38977767507"),
            "00 prefix pass" => array("0038977767507", "38977767507"),
//            "00 prefix fail" => array("0038977767507", "0038977767507"),
            "RegEx pass" => array ("38977767507", "38977767507")
        );
    }

    public function testSanitizeException()
    {
        $this->expectException(Exception::class);
        $this->testObj->Sanitize("389fsfdf4542");
    }

    /**
     * @dataProvider countryCodeProvider
     */
    public function  testCountryCode($input, $expected)
    {
        $this->testObj->country_code($input);
        $this->assertTrue($this->testObj->cc == $expected);
    }

    // Important: Each country code (CC) is not exact subsequence of (CC + MNO) of another country.
    public function countryCodeProvider()
    {
        return array(
            "len(MNO)=1"  => array("1204123456789", "1"),
            "len(MNO)=2" => array("2772123456789", "27"),
            "len(MNO)=3" => array("59895123456789", "598")
        );
    }

    public function testInvalidCountryCode()
    {
        $this->expectException(Exception::class);
        $this->testObj->country_code("29677123456789");
    }

    public function testMobileNetworkOperatorFixedPattern()
    {
        $this->testObj->cc = "389";
        $this->testObj->mobile_network_operator(substr("38977767507", strlen($this->testObj->cc)));
        $this->assertTrue($this->testObj->mno == "77");
    }

    public function testMobileNetworkOperatorVariablePrefixLength() // 731x , 7317x
    {
        $this->testObj->cc = "46";
        $this->testObj->mobile_network_operator(substr("467317123456", strlen($this->testObj->cc)));
        $this->assertTrue($this->testObj->mno == "73171");
    }

    public function testMobileNetworkOperatorMultipleISOCodes() // 262 6 -> Reunion (RE) , 262 639 -> Mayotte (YT)
    {
        $this->testObj->cc = "262";
        $this->testObj->mobile_network_operator(substr("262639123456", strlen($this->testObj->cc)));
        $this->assertTrue($this->testObj->iso_cc == "YT" and $this->testObj->mno == "639");
    }

    public function testMobileNetworkOperatorNANP()
    {
        $this->testObj->cc = "1";
        $this->testObj->mobile_network_operator(substr("1431123456", strlen($this->testObj->cc)));
        $this->assertTrue($this->testObj->iso_cc == "CA");
    }

    public function testMobileNetworkOperatorNANPUS() // US prefix = xxx
    {
        $this->testObj->cc = "1";
        $this->testObj->mobile_network_operator(substr("1310123456", strlen($this->testObj->cc)));
        $this->assertTrue($this->testObj->iso_cc == "US" and $this->testObj->mno == "310" );
    }

    public function testMobileNetworkOperatorAnyDigitPattern() // ex. MNO = xx
    {
        $this->testObj->cc = "855";
        $this->testObj->mobile_network_operator(substr("855123456", strlen($this->testObj->cc)));
        $this->assertTrue($this->testObj->iso_cc == "KH" and $this->testObj->mno == "12");
    }

    public function testMobileNetworkOperatorBrazil() // ex. MNO = xx68
    {
        $this->testObj->cc = "55";
        $this->testObj->mobile_network_operator(substr("55009123456", strlen($this->testObj->cc)));
        $this->testObj->subscriber_number("55009123456");
        $this->assertTrue($this->testObj->mno == "009" and $this->testObj->sn == "123456");
    }

    /**
     * @dataProvider mnoKosovoProvider
     */
    public function testMobileNetworkOperatorKosovo($input,$expected)
    {
        $this->testObj->cc = substr($input,0,3);
        $this->testObj->mobile_network_operator(substr($input, strlen($this->testObj->cc)));
        $this->assertSame($this->testObj->iso_cc, $expected);
    }

    /*Mobile phones in Kosovo may be on Serbian (country code +381) phone network on +381 28
    or through Monaco (+377) on +377 44 and +377 45
    or through Slovenia (+386) on +386 43 and +386 49.
    or since 15.12.2016 +383 country code is allocated*/
    public function mnoKosovoProvider()
    {
        return array(
            "SloveniaMNO"  => array("38649123456", "XK"),
            "SloveniaMNO fail"  => array("38640123456", "SI"),
            "SerbiaMNO" => array("38128123456", "XK"),
            "SerbiaMNO fail" => array("38160123456", "RS"),
            "MonacoMNO" => array("37745123456", "XK"),
            "MonacoMNO fail" => array("3774723456789", "MC"),
            "KosovoMNO" => array ("38399123456", "XK")
        );
    }

    public function  testUnknownMobileNetworkIdentifier()
    {
        $this->testObj->cc = "389";
        $this->expectException(Exception::class);
        $this->testObj->mobile_network_operator(substr("38979123456", strlen($this->testObj->cc)));
    }

    public function testSubscriberNumber()
    {
        $this->testObj->cc = "220";
        $this->testObj->mno = "92";
        $this->testObj->subscriber_number("22092123456");
        $this->assertTrue($this->testObj->sn == "123456");
    }

    public function  testInvalidSubscriberNumber()
    {
        $this->testObj->cc = "39";
        $this->testObj->mno = "06698";
        $this->expectException(Exception::class);
        $this->testObj->subscriber_number("3906698");
    }
}