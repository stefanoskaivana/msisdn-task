<?php

require_once __DIR__.'/vendor/autoload.php';
use JsonRPC\Client;

$client = new Client('http://msisdn.dev/server.php',true); //$returnException argument causes exceptions to be returned, default value=false
$client->getHttpClient()
    ->withUsername('user1')
    ->withPassword('password1');

$result = $client->decode('+467316123456');
echo $result;