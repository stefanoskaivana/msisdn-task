<?php

require_once __DIR__.'/vendor/autoload.php';
use MSISDNDecoder\MSISDN;

$msisdn = "";
$output = "";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $msisdn = test_input($_POST["msisdn"]);

    $m = new MSISDN();
    $output = $m->decode($msisdn);
    //echo $output;
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

require "index.html";
