<?php

require_once __DIR__.'/vendor/autoload.php';
use JsonRPC\Server;
use JsonRPC\MiddlewareInterface;
use JsonRPC\Exception\AccessDeniedException;
use MSISDNDecoder\MSISDN;


class MyMiddleware implements MiddlewareInterface
{
    public function execute($username, $password, $procedureName)
    {
        if ($procedureName != 'decode') {
            throw new AccessDeniedException('Access to procedure: ' . $procedureName . 'is denied');
        }
    }
}

$server = new Server();
$server->authentication(['user1' => 'password1', 'user2' => 'password2']); // AccessDeniedException
$server->getMiddlewareHandler()->withMiddleware(new MyMiddleware()); //Middleware might be used to authenticate and authorize the client. They are executed before each procedure.
$server->getProcedureHandler()->withObject(new MSISDN());

echo $server->execute();