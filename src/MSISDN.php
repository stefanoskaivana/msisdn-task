<?php

namespace MSISDNDecoder;

define('ROOTPATH', dirname(dirname(__FILE__)));

class MSISDN
{
    public $cc; // Country Code
    public $mno; // Mobile Network Operator identifier
    public $sn; // Subscriber Number
    public $iso_cc; // ISO 3166-1 alpha 2 Country Code

    public function __construct()
    {
        $this->cc = "";
        $this->mno = "";
        $this->sn = "";
        $this->iso_cc = "";
    }

    public function decode(string $msisdn)
    {
        try {
            $msisdn = $this->sanitize($msisdn);

            $this->country_code($msisdn);

            $this->mobile_network_operator(substr($msisdn, strlen($this->cc)));

            $this->subscriber_number($msisdn);
        }
        catch (\Exception $e) {
            return json_encode(array("Exception:" => $e->getMessage()));
        }

        $response = array("Country Code" => $this->cc, "MNO Identifier" => $this->mno, "Subscriber Number" => $this->sn, "ISO CC" => $this->iso_cc);
        return json_encode($response);
    }

    public function sanitize(string $s)
    {
        $s = str_replace(" ", "", $s);
        $prefixes = array("+", "00");
        for ($i = 0; $i < count($prefixes); $i++) {
            if (substr($s, 0, strlen($prefixes[$i])) == $prefixes[$i]) {
                $s = substr($s, strlen($prefixes[$i]));
            }
        }

        /*Minimum length of the MSISDN is not specified by ITU-T but is instead specified in the national numbering plans by the telecommunications regulator in each country.
        The shortest international phone numbers in use contain seven digits. -> https://www.safaribooksonline.com/library/view/regular-expressions-cookbook/9781449327453/ch04s03.html */
        if (!preg_match('/^[0-9]{7,15}$/', $s)) {
            throw new \Exception("MSISDN can contain between 7 and 15 digits, international call prefix is not included!", -1);
        }

        return $s;
    }

    public function country_code(string $s)
    {
        $filename = ROOTPATH.'/data/country-codes.json';
        if(!file_exists($filename)){
            throw new \Exception("File " . $filename . " doesn't exist!");
        }

        $str = file_get_contents($filename);
        if($str === FALSE) {
            throw new \Exception("Cannot access the file");
        }

        $json = json_decode(utf8_encode($str),true);
        if(!(json_last_error() === JSON_ERROR_NONE)) {
            throw new \Exception("Invalid json format");
        }
        else {
            foreach ($json as $k => $v){
                if($v["icc"] == substr($s, 0, strlen($v["icc"]))) {
                    $this->cc = $v["icc"];
                    //$this->iso_cc = $v["iso_cc"]; // ISO Code often depends on MNO i.e. several countries with different ISO Codes share same country calling code
                }
            }
        }

        if($this->cc == "") {
            throw new \Exception("Invalid country dialing code");
        }
    }

    public function mobile_network_operator(string $s)
    {
        $filename =  ROOTPATH.'/data/MNO-prefixes/' . $this->cc . '-mno.json';
        if(!file_exists($filename)){
            throw new \Exception("File " . $filename . " doesn't exist!");
        }

        $str = file_get_contents($filename);
        if($str === FALSE) {
            throw new \Exception("Cannot access the file");
        }

        $mnoData = json_decode($str, true);

        $ccAnyDigitPattern=""; // ex. xxx for USA
        $isoCCAnyDigitPattern="";
        foreach ($mnoData as $k => $v) {
            foreach ($v["prefixes"] as $c) {
                //AnyDigitPattern ($c in (x,xx,xxx,...))
                if(strlen(str_replace("x", "", $c, $i))==0) { // ex: xxx za USA -> AnyDigitPattern with length=3
                    $ccAnyDigitPattern=$c;
                    $isoCCAnyDigitPattern = $v["iso_cc"];
                    break;
                }

               $start = 0;
               if (preg_match('/^x+\d+$/', $c)) {  // if MNO starts with any digit/s + fixed sequence of digits, ex. xx96 (MNO in  Brazil)
                   $start = substr_count($c,"x");
               }
               $c = str_replace("x", "", $c, $i);
               if ($c == substr($s, $start, strlen($c)) and strlen($c) + $i >= strlen($this->mno)) { // ex. xx96 & xx9
                   $this->mno = substr($s, 0, strlen($c) + $i);
                   $this->iso_cc = $v["iso_cc"];
               }
            }
        }

        if($this->mno=="" and $ccAnyDigitPattern !="") {
            $this->mno = substr($s, 0, strlen($ccAnyDigitPattern));
            $this->iso_cc = $isoCCAnyDigitPattern;
        }
        else if($this->mno == "") {
            throw new \Exception("Invalid MNO identifier");
        }
    }

    public function subscriber_number(string $s){
        //MSISDN = CC + MNO + SN;
        $start = strlen($this->cc) + strlen($this->mno);
        $end = 15; // max_lenght(MSISDN)=15;
        $this->sn = substr($s, $start, $end-$start);

        if($this->sn == "") {
            throw new \Exception("Invalid subscriber number");
        }
    }
}